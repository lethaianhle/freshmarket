package config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import entity.Category;

public class DBConfig {
    public static void main(String[] args) {
        // create three connections to three different databases on localhost
        Connection conn1 = null;
        Connection conn2 = null;
        Connection conn3 = null;
 
        try {
//            // Connect method #1
//            String dbURL1 = "jdbc:postgresql:ProductDB1?user=root&password=secret";
//            conn1 = DriverManager.getConnection(dbURL1);
//            if (conn1 != null) {
//                System.out.println("Connected to database #1");
//            }
// 
//            // Connect method #2
//            String dbURL2 = "jdbc:postgresql://localhost/ProductDB2";
//            String user = "root";
//            String pass = "secret";
// 
//            conn2 = DriverManager.getConnection(dbURL2, user, pass);
//            if (conn2 != null) {
//                System.out.println("Connected to database #2");
//            }
 
            // Connect method #3
            String dbURL3 = "jdbc:postgresql://localhost:5432/FreshMarket";
            Properties parameters = new Properties();
            parameters.put("user", "postgres");
            parameters.put("password", "Tunglam@01");
 
            conn3 = DriverManager.getConnection(dbURL3, parameters);
            if (conn3 != null) {
                System.out.println("Connected to database #3");
            }
            
                // Step 2:Create a statement using connection object
                PreparedStatement preparedStatement = conn3.prepareStatement("select * from public.categories");
                System.out.println(preparedStatement);
                // Step 3: Execute the query or update query
                ResultSet rs = preparedStatement.executeQuery();

                // Step 4: Process the ResultSet object.
            	Category c = new Category();

                while (rs.next()) {
                    c.id = rs.getString("Id");
                  	c.name = rs.getString("Name");
                
                }
                System.out.print(c.id+ c.name);
 
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (conn1 != null && !conn1.isClosed()) {
                    conn1.close();
                }
                if (conn2 != null && !conn2.isClosed()) {
                    conn2.close();
                }
                if (conn3 != null && !conn3.isClosed()) {
                    conn3.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

}

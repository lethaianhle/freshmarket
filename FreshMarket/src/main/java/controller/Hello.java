package controller;

import java.io.IOException;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Category;

/**
 * Servlet implementation class Hello
 */
@WebServlet("/Hello")
public class Hello extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Hello() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        // Step 1: Establishing a Connection
		 String dbURL = "jdbc:postgresql://localhost:5432/FreshMarket";
         Properties parameters = new Properties();
         parameters.put("user", "postgres");
         parameters.put("password", "Tunglam@01");
         Connection conn3;
		try {
			conn3 = DriverManager.getConnection(dbURL, parameters);
			 PreparedStatement preparedStatement = conn3.prepareStatement("select * from public.categories");
	         System.out.println(preparedStatement);
	            // Step 3: Execute the query or update query
	         ResultSet rs = preparedStatement.executeQuery();

	            // Step 4: Process the ResultSet object.
	        Category c = new Category();

	            while (rs.next()) {
	                c.id = rs.getString("uuid");
	              	c.name = rs.getString("Name");
	            
	            }
	            System.out.print(c.id+ c.name);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
            // Step 2:Create a statement using connection object
        
	}

	private void printSQLException(SQLException e) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
